package com.ecommerce.cartmain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring boot ecommerce main class.
 * 
 * @author victor
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = { "com.ecommerce.cache", "com.ecommerce.exceptions", "com.ecommerce.model",
		"com.ecommerce.controller", "com.ecommerce.service" })
@EnableCaching
public class CartMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartMainApplication.class, args);
	}
}
