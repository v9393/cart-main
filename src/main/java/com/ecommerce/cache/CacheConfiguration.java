package com.ecommerce.cache;

/**
 * Cache class for configuration values
 * 
 * @author victor
 *
 */
public class CacheConfiguration {

	private static final int CACHE_TTL_TIME_MINUTES = 10;

	/**
	 * Get for CACHE_TTL_TIME_MINUTES property
	 * 
	 * @return time in minutes TTL
	 */
	public static int getCacheTtl() {
		return CACHE_TTL_TIME_MINUTES;
	}
}
