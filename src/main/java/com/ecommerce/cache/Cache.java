package com.ecommerce.cache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecommerce.model.Cart;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Public class cache. Stores Cart objects using key, with TTL of 10 minutes.
 * 
 * @author victor
 *
 */
public class Cache {

	private static final Logger log = LoggerFactory.getLogger(Cache.class);

	LoadingCache<String, Cart> loadingCache;

	public int cacheExpirationTime;

	/**
	 * Cache Constructor. Initializes loadingCache object
	 */
	public Cache() {
		this.cacheExpirationTime = CacheConfiguration.getCacheTtl();
		log.info("TTL time from cache cacheExpirationTime: " + cacheExpirationTime + " MINUTES");
		loadingCache = CacheBuilder.newBuilder().expireAfterWrite(this.cacheExpirationTime, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Cart>() {
					@Override
					public Cart load(String key) throws Exception {
						return returnVal(key);
					}
				});

	}

	/**
	 * Return passed key
	 * 
	 * @param s Key received
	 * @return Same value as s
	 */
	public Cart returnValProd(Cart s) {
		return s;
	}

	/**
	 * Function runs when load fail
	 * 
	 * @param s key param passed to cache map
	 * @return Always return null
	 */
	public Cart returnVal(String s) {
		Cart cart = null;
		if (cart == null) {
			log.error("Cache - returnval - Cart equals null");
		}
		return cart;
	}

	/**
	 * Stores cart in cache from a key
	 * 
	 * @param key  Key value from stored object
	 * @param cart Object stored
	 */
	public void putCacheVal(String key, Cart cart) {
		loadingCache.put(key, cart);
	}

	/**
	 * Return cached value from passed key
	 * 
	 * @param key Get value from key
	 * @return If key exists return that object, if not returns null
	 */
	public Cart getCacheVal(String key) {
		try {
			return loadingCache.get(key);
		} catch (ExecutionException e) {
			return null;
		}
	}

	/**
	 * Delete key from cache.
	 * 
	 * @param key Selected key from delete
	 */
	public void deleteKeyFromCache(String key) {
		loadingCache.invalidate(key);

	}

	/**
	 * LoadingCache getter function
	 * 
	 * @param key Selected key from delete
	 */
	public LoadingCache<String, Cart> getLoadingCache() {
		return loadingCache;
	}

	/**
	 * LoadingCache setter function
	 * 
	 * @param key Selected key from delete
	 */
	public void setLoadingCache(LoadingCache<String, Cart> loadingCache) {
		this.loadingCache = loadingCache;
	}

	public void updateProductCache(int idCart, Cart cart) {
		this.putCacheVal(String.valueOf(idCart), cart);
	}

}
