package com.ecommerce.service;

import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ecommerce.cache.Cache;
import com.ecommerce.controller.CartController;
import com.ecommerce.exceptions.CartNotFoundException;
import com.ecommerce.model.Cart;
import com.ecommerce.model.CartsRepository;
import com.ecommerce.model.Product;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;

/**
 * Carts Service
 * 
 * @author victor
 *
 */
@Service
public class CartService {

	private static final Logger log = LoggerFactory.getLogger(CartController.class);

	private CartsRepository cartsList;

	private Cache cache;

	/**
	 * Constructor
	 */
	public CartService() {
		this.cache = new Cache();
		this.cartsList = new CartsRepository();
	}

	/**
	 * Method that allows creation of new carts
	 * 
	 * @return Cart Model
	 */
	public Cart createNewCart() {
		Cart cart = cartsList.createNewCart();
		cache.putCacheVal(String.valueOf(cart.getId()), cart);
		return cart;
	}

	/**
	 * Gets cart from cache
	 * 
	 * @param id Cart id
	 * @return Cart Model
	 * @throws ExecutionException
	 * @throws CartNotFoundException
	 */
	public Cart getCartFromIdFromCache(int id) throws ExecutionException, CartNotFoundException {
		Cart cart = null;
		try {
			cart = this.getCache().getCacheVal(String.valueOf(id));
		} catch (InvalidCacheLoadException ex) {
			log.error("CartService - getCartFromIdFromCache - InvalidCacheLoadException");
			throw new CartNotFoundException(id);
		} catch (NullPointerException ex) {
			log.error("CartService - getCartFromIdFromCache - NullPointerException");
			throw new CartNotFoundException(id);
		}

		if (cart == null) {
			throw new CartNotFoundException(id);
		}

		return cart;
	}

	/**
	 * Deletes cart from cache
	 * 
	 * @param id cart will be deleted
	 */
	public void deleteCartList(int id) {
		this.getCache().deleteKeyFromCache(String.valueOf(id));
	}

	/**
	 * Method that allows to put product in cart
	 * 
	 * @param foundProduct product to add
	 * @param idCart       Cart that adds product
	 * @param quantity     Number of repetitions of product
	 * @return Cart Model
	 * @throws CartNotFoundException
	 */
	public Cart addProductToCart(Product foundProduct, int idCart, String quantity) throws CartNotFoundException {
		Cart cart = null;
		int quant = 1;
		if (!"".equals(quantity) && quantity != null) {
			quant = Integer.valueOf(quantity);
		}

		try {
			cart = getCartFromIdFromCache(idCart);
			for (int x = 0; x < quant; x++) {
				cart.addProductToCart(foundProduct);
			}

			cache.updateProductCache(idCart, cart);
		} catch (ExecutionException e) {
			log.error("CartService - addProductToCart - ExecutionException");
		}

		return cart;
	}

	private Cache getCache() {
		return this.cache;
	}

}
