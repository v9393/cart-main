package com.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.exceptions.ProductNotFoundException;
import com.ecommerce.model.AvailableProducts;
import com.ecommerce.model.Product;

import lombok.Getter;
import lombok.Setter;

/**
 * Products Service class
 * 
 * @author victor
 *
 */
@Service
@Getter
@Setter
public class ProductService {

	@Autowired
	private AvailableProducts availableProducts;

	/**
	 * Return lists of products
	 * 
	 * @return Products array
	 */
	public Product[] getProductList() {
		return availableProducts.getProducts();
	}

	/**
	 * Given id, returns products that matches that id
	 * 
	 * @param id Id stored product
	 * @return product that matches id
	 */
	public Product findProductById(int id) throws ProductNotFoundException {
		Product product = null;
		product = availableProducts.foundProductById(id);
		if (product == null) {
			throw new ProductNotFoundException(id);
		}
		return product;
	}

}
