package com.ecommerce.controller;

import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.exceptions.CartNotFoundException;
import com.ecommerce.exceptions.ProductNotFoundException;
import com.ecommerce.model.Cart;
import com.ecommerce.model.Product;
import com.ecommerce.service.CartService;
import com.ecommerce.service.ProductService;

/**
 * Controller class for cart functions
 * 
 * @author victor
 *
 */
@RestController
public class CartController {

	private static final Logger log = LoggerFactory.getLogger(CartController.class);

	@Autowired
	private CartService cartsService;

	@Autowired
	private ProductService productService;

	/**
	 * Cart endpoint for creating a new cart
	 * 
	 * @return Response Entity Cart
	 */
	@PostMapping("/cart")
	public ResponseEntity<Cart> createCart() {
		Cart createdCart = cartsService.createNewCart();
		log.info("CartController - createCart - cartCreated with id: " + createdCart.getId());
		return ResponseEntity.ok(createdCart);
	}

	/**
	 * Cart endpoint. Get information about cart
	 * 
	 * @param id    Requested cart
	 * @param model holder for model attributes
	 * @return Selected Entity Cart
	 */
	@GetMapping("/cart/{id}")
	public ResponseEntity<Cart> getCart(@PathVariable("id") int id, Model model) {
		Cart foundCart = null;

		try {
			try {
				foundCart = cartsService.getCartFromIdFromCache(id);
			} catch (CartNotFoundException e) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
		} catch (ExecutionException e) {
			log.error("CartController - getCart - ExecutionException: ");
		}

		log.info("CartController - getCart - returns cart with id: " + foundCart.getId());
		return ResponseEntity.ok(foundCart);
	}

	/**
	 * Endpoint carts deletion
	 * 
	 * @param id Selected cart to remove
	 * @return Void
	 */
	@DeleteMapping("/cart/{id}")
	public ResponseEntity<Object> deleteCart(@PathVariable int id) {
		cartsService.deleteCartList(id);
		log.info("CartController - deleteCart - deleted cart with id: " + id);
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	/**
	 * Endpoint that add products to a cart
	 * 
	 * @param idCart    Cart will be added
	 * @param idProduct Product that will be added
	 * @param request   Accepts quantity parameter
	 * @return Response Entity with cart information
	 */
	@PostMapping("/cart/{idCart}/product/{idProduct}")
	public ResponseEntity<Cart> addProductToCart(@PathVariable int idCart, @PathVariable int idProduct,
			HttpServletRequest request) {
		Cart foundCart = null;
		Product foundProduct;
		String quantity = request.getParameter("quantity");
		try {

			foundProduct = productService.findProductById(idProduct);
		} catch (ProductNotFoundException ex) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		try {
			foundCart = cartsService.getCartFromIdFromCache(idCart);
		} catch (ExecutionException ex) {
			log.error("CartController - addProductToCart: " + ex.getMessage());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		} catch (CartNotFoundException ex) {
			log.error("CartController - addProductToCart: CartNotFoundException");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		try {
			foundCart = cartsService.addProductToCart(foundProduct, idCart, quantity);
		} catch (CartNotFoundException ex) {
			log.error("CartController - addProductToCart: CartNotFoundException");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}

		log.info("CartController - addProductToCart - add " + foundProduct.getDescription() + " into cart with id: "
				+ idCart);
		return ResponseEntity.ok(foundCart);
	}

}
