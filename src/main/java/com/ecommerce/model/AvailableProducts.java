package com.ecommerce.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class handles products from json
 * 
 * @author victor
 *
 */
@Component
public class AvailableProducts {

	Product[] products;

	@Value("${products.resource}")
	public String productResource;

	HashMap<String, Product> productList = new HashMap<String, Product>();

	/**
	 * After the object is created, gets the products information
	 * 
	 * @throws IOException
	 */
	@PostConstruct
	public void init() throws IOException {
		File resource = new ClassPathResource(productResource).getFile();
		String jsonProducts = new String(Files.readAllBytes(resource.toPath()));

		ObjectMapper objectMapper = new ObjectMapper();
		products = objectMapper.readValue(jsonProducts, Product[].class);

		for (Product product : products) {
			productList.put(String.valueOf(product.getId()), product);
		}

	}

	public Product[] getProducts() {
		return products;
	}

	public void setProducts(Product[] products) {
		this.products = products;
	}

	public HashMap<String, Product> getProductList() {
		return productList;
	}

	public void setProductList(HashMap<String, Product> productList) {
		this.productList = productList;
	}

	public Product foundProductById(int id) {
		return productList.get(String.valueOf(id));
	}

}
