package com.ecommerce.model;

public class CartsRepository {

	public int lastId = 0;

	public CartsRepository() {
	}

	public Cart createNewCart() {
		Cart cartNew = new Cart(lastId);
		this.updateId();
		return cartNew;
	}

	public void updateId() {
		this.lastId += 1;
	}

}
