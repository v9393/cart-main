package com.ecommerce.model;

import java.util.ArrayList;

public class Cart {

	private int id;
	private String description;
	private ArrayList<Product> productList;

	public Cart(int currentId) {
		this.id = currentId;
		this.productList = new ArrayList<Product>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void addProductToCart(Product product) {
		this.productList.add(product);
	}

	public String toStringProducts() {

		String products = "";
		if (!this.productList.isEmpty()) {
			for (Product prod : this.productList) {
				products += prod.getDescription();
			}
		}
		return products;

	}

	public ArrayList<Product> getProductList() {
		return productList;
	}

}
