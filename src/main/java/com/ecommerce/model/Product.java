package com.ecommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;
import lombok.Data;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@Data public class Product {

	private String id;
	private double amount;
	private String description;
	
	public Product() {
		super();
	}
	
	public Product(String description, double amount, String id) {
		this.description = description;
		this.amount = amount;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "Description: " + this.description + " amount of: " + this.amount + " id: " + this.id;
	}
}
