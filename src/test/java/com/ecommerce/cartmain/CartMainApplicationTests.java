package com.ecommerce.cartmain;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ecommerce.exceptions.CartNotFoundException;
import com.ecommerce.model.Cart;
import com.ecommerce.service.CartService;

@SpringBootTest
class CartMainApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Autowired
	private CartService cartService;
	
	@Test
	public void createCart_cartShouldBeCreated() {
		Cart exampleCart = cartService.createNewCart();
		assertEquals(exampleCart.getId(),0);
		assertEquals(exampleCart.getProductList().size(),0);
	}
	
	@Test
	public void createCart_cartStoredInCache() {
		Cart exampleCart = cartService.createNewCart();
		int cartId = exampleCart.getId();
		Cart cartFound = null;
		try {
			cartFound = cartService.getCartFromIdFromCache(cartId);
		} catch (ExecutionException e) {

		} catch (CartNotFoundException e) {
		}
		assertEquals(cartFound.getId(),cartId);
	}
	

}
