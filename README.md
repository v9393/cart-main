# Backend API #

The challengeaccepted-commerce application allows you to consume the API for your e-commerce. 
This application brings you support for create your cart, add your products and delete your cart if you want.

## Use cases

### Managing your cart
* Add your cart using cart endpoint:


```bash
	POST /cart
```

Expected response example:
```bash
{
    "id": 0,
    "description": null,
    "productList": []
}
```



* Get a cart information passing its id:


```bash
	GET /cart/2
```

Expected response example:
```bash
{
    "id": 0,
    "description": null,
    "productList": []
}
```


* Delete a cart passing its id:


```bash
	DEL /cart/2
```

Void response expected.


* Adding product at a cart.

Supports x-www-form-urlencoded param named "quantity". Allows you to add quantity times product in a cart.



```bash
	POST /cart/0/product/1
```


Expected response example:
```bash
{
    "id": 0,
    "description": null,
    "productList": [
        {
            "id": "1",
            "amount": 3.0,
            "description": "Crisps"
        }
    ]
}
```

### Products

Products are stored in a json file at /resources path.

### Memory

Carts are stored in memory and if you don't use, will be deleted at 10 minutes.

### Test it

Test it using postman's collections!
